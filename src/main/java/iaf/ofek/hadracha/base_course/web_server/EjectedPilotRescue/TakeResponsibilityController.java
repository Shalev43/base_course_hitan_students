package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.Data.CrudDataBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController()
@RequestMapping("ejectedPilotRescue/")
public class TakeResponsibilityController {

    @Autowired
    TakeResponsibilityService takeResponsibilityService;


    @GetMapping("takeResponsibility")
    public void get(@RequestParam int ejectionId, @CookieValue(value = "client-id", defaultValue = "") String clientId) {
        System.out.println();
        takeResponsibilityService.takeResponsibility(ejectionId ,clientId);
    }

}



