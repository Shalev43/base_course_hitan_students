package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.Data.CrudDataBase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
@RequestMapping("ejectedPilotRescue/infos")
public class EjectionsExporterController {

  private CrudDataBase dataBase;

  public EjectionsExporterController(@Autowired CrudDataBase dataBase) {
    this.dataBase = dataBase;
  }

  @GetMapping
  public List<EjectedPilotInfo> getLaunches() {
    return dataBase.getAllOfType(EjectedPilotInfo.class);
  }
}
