package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.Data.CrudDataBase;
import iaf.ofek.hadracha.base_course.web_server.Utilities.ListOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class TakeResponsibilityService {

  @Autowired AirplanesAllocationManager airplanesAllocationManager;
  private final CrudDataBase dataBase;

  public TakeResponsibilityService(@Autowired CrudDataBase dataBase) {
    this.dataBase = dataBase;
  }

  public void takeResponsibility(int ejectionId, String clientId) {
    EjectedPilotInfo ejectedPilotInfoById = dataBase.getByID(ejectionId, EjectedPilotInfo.class);

    if (ejectedPilotInfoById.getRescuedBy() == null) {
      ejectedPilotInfoById.setRescuedBy(clientId);
      dataBase.update(ejectedPilotInfoById);

      sendAllocateAirplanes(ejectedPilotInfoById, clientId);
    }
  }

  private void sendAllocateAirplanes(
      EjectedPilotInfo currentEjectedPilotInfo, String controllerId) {
    this.airplanesAllocationManager.allocateAirplanesForEjection(
        currentEjectedPilotInfo, controllerId);
  }
}
